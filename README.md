# Description

This is the openshift infra part of the loaderio-token-server.

# Instruction

1. `oc login` to your cluster and `oc project` to your project running the web server to be load tested.

2. Copy an existing overlay to a copy with a suitable name:
  `cp -r kustomize/overlays/joblinks-search-api-staging kustomize/overlays/your-nice-new-name`

3. Edit `kustomize/overlays/your-nice-new-name/route.yaml`:
```
  # In the top route object, edit host, namespace, and name (avoid name collisions
  # with any existing routes deployed in openshift).

  # In the bottom route object, edit namespace and host (copy from host in the top route object).

  # Also set your loaderio token here:
  path: /loaderio-xxxxxxxxxxxxxxxx.txt
```
4. Edit `kustomize/overlays/your-nice-new-name/kustomization.yaml` and change to correct namespace

5. `oc apply -k kustomize/overlays/your-nice-new-name`

6. Add, commit and push your new overlay for future reference.

7. Create a textfile with the following content without newline:
   `echo -n 'loaderio-xxxxxxxxxxxxxxxx' > /tmp/loaderio-xxxxxxxxxxxxxxxx.txt`

8. Copy the textfile to S3:
   `aws s3 cp /tmp/loaderio-xxxxxxxxxxxxxxxx.txt  s3://data.jobtechdev.se-adhoc/public-tokens/`

# VERY IMPORTANT NOTE

You cannot use TLS/https when load testing, because OCP cannot resolve the request host+path
to the right route if using https.

The solution is to change to http (remove TLS etc) from your
application's route.yaml while running the load testing.
